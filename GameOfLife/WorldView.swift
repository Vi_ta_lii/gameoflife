//
//  WorldView.swift
//  GameOfLife
//
//  Created by Me on 3/22/19.
//  Copyright © 2019 VitaliiKravets. All rights reserved.
//

import UIKit

class WorldView: UIView {

	let world: World
	
	init(world: World) {
		self.world = world
		super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
    override func draw(_ rect: CGRect) {
		
		let context = UIGraphicsGetCurrentContext()
		self.world.cells.forEach {
			context?.setFillColor(self.color(for: $0.state).cgColor)
			context?.addRect(self.frame(for: $0))
			context?.fillPath()
		}
    }
	
	func configure(with bounds: CGRect) {
		let margin: CGFloat = 20.0
		let size = bounds.width - margin * 2.0
		let frame = CGRect(x: margin, y: (bounds.height - size) / 2.0, width: size, height: size)
		self.frame = frame
		self.layer.borderColor = UIColor.darkGray.cgColor
		self.layer.borderWidth = 2.0
	}
	
	private func color(for cellState: CellState) -> UIColor {
		switch cellState {
		case .alive: return .black
		case .dead:  return .lightGray
		}
	}
	
	private func frame(for cell: Cell) -> CGRect {
		let broadness = CGFloat(self.world.broadness)
		let width     = self.bounds.width / broadness
		let height    = self.bounds.height / broadness
		
		return CGRect(x: CGFloat(cell.x) * width,
					  y: CGFloat(cell.y) * height,
					  width: width,
					  height: height)
	}
}
