//
//  ViewController.swift
//  GameOfLife
//
//  Created by Me on 3/21/19.
//  Copyright © 2019 VitaliiKravets. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	let world = World()
	let worldView: WorldView
	var timer = Timer()
	
	required init?(coder aDecoder: NSCoder) {
		self.worldView = WorldView(world: self.world)
		super.init(coder: aDecoder)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.worldView.configure(with: self.view.bounds)
		self.view.addSubview(self.worldView)
		self.addGestureRecognizers()
	}
	
	@objc private func fireOffTimer() {
		self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.iterateWorld), userInfo: nil, repeats: true)
	}
	
	@objc private func resetCivilization() {
		self.timer.invalidate()
		self.world.reset()
		self.worldView.setNeedsDisplay()
	}
	
	@objc private func iterateWorld() {
		self.world.iterate()
		self.worldView.setNeedsDisplay()
	}
	
	private func addGestureRecognizers() {
		let doubleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.resetCivilization))
		doubleTapGestureRecognizer.numberOfTapsRequired = 2
		
		let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.fireOffTimer))
		tapGestureRecognizer.numberOfTapsRequired = 1
		tapGestureRecognizer.require(toFail: doubleTapGestureRecognizer)
		
		[tapGestureRecognizer, doubleTapGestureRecognizer].forEach { self.view.addGestureRecognizer($0) }
	}
}

