//
//  Rules.swift
//  GameOfLife
//
//  Created by Me on 3/21/19.
//  Copyright © 2019 VitaliiKravets. All rights reserved.
//

import Foundation

struct Rules {
	
	let aliveNeighbors = 3
	let aliveNeighborsRange = 2...3
}
