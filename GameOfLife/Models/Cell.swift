//
//  Cell.swift
//  GameOfLife
//
//  Created by Me on 3/21/19.
//  Copyright © 2019 VitaliiKravets. All rights reserved.
//

import Foundation

enum CellState {
	
	case alive
	case dead
}

class Cell {
	
	let x: Int
	let y: Int
	var state: CellState
	
	init(_ x: Int, _ y: Int) {
		self.x = x
		self.y = y
		self.state = .dead
	}
	
	func switchState() {
		self.state = (self.state == .alive) ? .dead : .alive
	}
}
