//
//  World.swift
//  GameOfLife
//
//  Created by Me on 3/21/19.
//  Copyright © 2019 VitaliiKravets. All rights reserved.
//

import Foundation

class World {
	
	let broadness = 20
	let rules = Rules()
	private (set) var cells: [Cell]
	
	init() {
		self.cells = [Cell]()
		
		for x in 0..<broadness {
			for y in 0..<broadness {
				self.cells.append(Cell(x, y))
			}
		}
		
		self.createRandomCells()
	}
	
	subscript (x: Int, y: Int) -> Cell? {
		return self.cells.filter {
			$0.x == x && $0.y == y
		}.first
	}
	
	func iterate() {
		
		self.cells.filter  { $0.state == .dead }
			.filter  { aliveNeighbors(for: $0) == self.rules.aliveNeighbors }
			.forEach { $0.switchState() }
		
		self.cells.filter  { $0.state == .alive }
			.filter  { !(self.rules.aliveNeighborsRange ~= aliveNeighbors(for: $0)) }
			.forEach { $0.switchState() }
	}
	
	func reset() {
		for x in 0..<broadness {
			for y in 0..<broadness {
				self[x, y]?.state = .dead
			}
		}
		
		self.createRandomCells()
	}
	
	private func createRandomCells() {
		(0...100).forEach { _ in
			let (x, y) = self.randomCellLocation()
			self[x, y]?.state = .alive
		}
	}
	
	private func randomCellLocation() -> (Int, Int) {
		return (Int.random(in: 0..<self.broadness), Int.random(in: 0..<self.broadness))
	}
	
	private func aliveNeighbors(for cell: Cell) -> Int {

		return self.cells.filter { areNeighbors(cell, $0) }
			.filter { $0.state == .alive }
			.count
	}
	
	private func areNeighbors(_ cell1: Cell, _ cell2: Cell) -> Bool {
		
		let distance = (abs(cell1.x - cell2.x), abs(cell1.y - cell2.y))
		switch distance {
		case (1,1), (1,0), (0,1): return true
		default: 				  return false
		}
	}
}
